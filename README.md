# iCE Bling – LED Earrings with the Lattice iCE40UP5k FPGA

![iCE Bling](ice-bling-sm.jpg)

This is a experimental project by Electronut Labs.

## What is iCE Bling ? 

iCE Bling is an Lattice iCE40UP5k FPGA based LED earrings. The earrings have 8x8 grid LEDs and are powered by CR2032 coin cell.

## The Design of iCE Bling

Here is link to the blog, [iCE Bling – Making LED Earrings with an FPGA](https://electronut.in/ice-bling-making-led-earrings-with-an-fpga/), on our website which will give you detailed information about iCE Bling..

# Software LICENSE 

All code in this repository is released under the MIT License below.

Copyright 2019 Electronut Labs (info@electronut.in)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Hardware License 

All hardware design files in this repository are released under the Creative Commons License.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.

### Code Repository 

You can find the design files and code for the iCE Bling here.

https://gitlab.com/electronutlabs-public/ice-bling

## About Electronut Labs

**Electronut Labs** is an Embedded Systems company based in Bangalore, India. More 
information at our [website](https://electronut.in).
