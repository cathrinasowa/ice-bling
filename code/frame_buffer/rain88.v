/*

    rain88.v

    Generate a rain-like pattern on 8x8 grid.

*/

`default_nettype none

module rain88 (
    input resetn,
    input clk,
    output [63:0] fb
);

reg [21:0] refresh_counter;

// data grid
reg [63:0] grid;
assign fb = grid;

always @ (posedge clk)
    begin 
        if (!resetn)
            begin

                // reset counters
                refresh_counter <= 0;

                // initialise grid
                grid[0:7]   <= 8'b01010101;    
                grid[8:15]  <= 8'b10101010; 
                grid[16:23] <= 8'b01010101; 
                grid[24:31] <= 8'b10101010; 
                grid[32:39] <= 8'b01010101; 
                grid[40:47] <= 8'b10101010; 
                grid[48:55] <= 8'b01010101; 
                grid[56:63] <= 8'b10101010; 
            end

        // update refresh counter 
        refresh_counter <= refresh_counter + 1;

        // update frame
        if (!refresh_counter)
            begin 
                //grid <= {grid[0], grid[62:1]};
                grid <= ~grid;
            end
    end

endmodule
