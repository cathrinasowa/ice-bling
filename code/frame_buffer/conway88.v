/*

    Conway's Game of Life on an 8x8 grid.

*/

`default_nettype none

module conway88 (
    input resetn,
    input clk,
    output [63:0] fb
);

// grid
reg [63:0] grid;
assign fb = grid;

// keep a copy 
reg [63:0] grid_cpy;

// GOF state machine
parameter sSTART        = 4'd0;
parameter sNEW_FRAME    = 4'd1;
parameter sUPDATE       = 4'd2;
parameter sCOMPUTE_8NN  = 4'd3;
reg [1:0] curr_state;

// counters
reg [21:0] cnewframe;

// current position in grid
reg [5:0] i;

// sum of values
reg [2:0] sum;

// 8x8 grid
parameter N = 8;

always @ (posedge clk)
    begin 
        if (!resetn)
            begin
                curr_state <= sSTART;
                cnewframe <= 0;
                i <= 0;
                sum <= 0;

                // add glider
                grid[0:7]   <= 8'b00100000;    
                grid[8:15]  <= 8'b10100000; 
                grid[16:23] <= 8'b01100000; 
                grid[24:31] <= 8'b00000000; 
                grid[32:39] <= 8'b00000000; 
                grid[40:47] <= 8'b00000000; 
                grid[48:55] <= 8'b00000000; 
                grid[56:63] <= 8'b00000000; 
            end

        // increment counter
        cnewframe <= cnewframe + 1;

        // handle state machine
        case (curr_state)

            sSTART:
                begin 
                    // start new frame
                    curr_state <= sNEW_FRAME;
                end

            sNEW_FRAME:
                begin 
                    // start new frame
                    if (!cnewframe)
                        begin
                            // copy grid
                            grid_cpy <= grid;

                            // reset current position
                            i <= 0;

                            // change state to compute sum
                            curr_state <= sCOMPUTE_8NN;
                        end
                end

            sUPDATE:
                begin 

                    // increment position 
                    i <= i + 1;

                    // set grid value based on sum
                    if (grid[i])
                        begin 
                            if (sum < 2 || sum > 3)
                                begin 
                                    grid[i] <= 0;
                                end
                        end 
                    else if (sum == 3) 
                        begin 
                            grid[i] <= 1;   
                        end
                    
                    // done with frame?
                    if (i == 63)
                        begin 
                            // go to new frame
                            curr_state <= sNEW_FRAME;
                        end
                    else 
                        begin 
                            // reset sum
                            sum <= 0;
                            // change state to compute
                            curr_state <= sCOMPUTE_8NN;
                        end
                end

            sCOMPUTE_8NN:
                begin 
                    // automatic toroidal boundary conditions because of 
                    // index overflow
                    sum <=  grid_cpy[i-N-1] + grid_cpy[i-N] + grid_cpy[i-N+1] + 
                            grid_cpy[i-1] + grid_cpy[i+1] + 
                            grid_cpy[i+N-1] + grid_cpy[i+N] + grid_cpy[i+N+1];

                    // change state to update
                    curr_state <= sUPDATE;
                end

            default:
                curr_state <= sSTART;

        endcase
    end
endmodule